package SistemaBibliotecario;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class TestPruebasFuncionamiento {

    private Biblioteca biblioteca;
    private UsuarioAdministrador usuarioAdmin;
    private UsuarioCliente usuarioCliente;
    private Libro libro;
    private Prestamo prestamo;

    @BeforeEach
    void PruebaDatos() throws Exception {
        biblioteca = new Biblioteca("Biblioteca", "Direccion 1");
        usuarioAdmin = new UsuarioAdministrador("A01", "Admin", "Direccion 2");
        usuarioCliente = new UsuarioCliente("U01", "User", "Direccion 3");
        libro = new Libro("L01", "1984", "Distopía", "George Orwell", 12);

        usuarioAdmin.agregarLibroBiblioteca(biblioteca, libro);
        usuarioAdmin.registrarUsuario(biblioteca, usuarioCliente);
    }

    @Test
    void testAgregarYEliminarLibro() throws Exception {
        assertEquals(1, biblioteca.getCatalogoLibros().size(), "El libro no fue agregado");

        usuarioAdmin.eliminarLibroBiblioteca(biblioteca, libro.getIdLibro());
        assertTrue(biblioteca.getCatalogoLibros().isEmpty(), "El libro no fue eliminado");
    }

    @Test
    void testRegistrarYEliminarUsuario() throws Exception {
        assertEquals(1, biblioteca.getUsuariosRegistrados().size(), "El usuario no fue registrado");

        biblioteca.eliminarUsuario(usuarioCliente.getIdUsuario());
        assertTrue(biblioteca.getUsuariosRegistrados().isEmpty(), "El usuario no fue eliminado");
    }

    @Test
    void testRealizarPrestamo() throws Exception {
        prestamo = new Prestamo(usuarioCliente, libro);
        assertEquals("Prestado", libro.getEstado(), "El estado del libro deberia ser Prestado");
        assertTrue(usuarioCliente.getLibrosPrestados().contains(libro), "El libro deberia estar en la lista de libros prestados");
    }

    @Test
    void testFinalizarPrestamo() throws Exception {
        prestamo = new Prestamo(usuarioCliente, libro);
        prestamo.finalizarPrestamo();
        assertEquals("Disponible", libro.getEstado(), "El estado del libro deberia ser Disponible");
        assertFalse(usuarioCliente.getLibrosPrestados().contains(libro), "El libro no deberia estar en la lista de libros prestados al finalizar el prestamo");
    }

    @Test
    void testRenovarPrestamo() throws Exception {
        prestamo = new Prestamo(usuarioCliente, libro);
        prestamo.renovarPrestamo();
        assertEquals(7, prestamo.getDiasPrestamo(), "Los dias de prestamo deberian ser restablecidos al renovarse");
    }

    @Test
    void testAñadirReseña() throws Exception {
        Reseña reseña = new Reseña("Buen Libro", 5, usuarioCliente, libro);
        assertEquals(1, libro.getReseñas().size(), "La reseña no se añadio correctamente");
    }

    @Test
    void testEliminarReseña() throws Exception {
        Reseña reseña = new Reseña("Regular Libro", 3, usuarioCliente, libro);
        libro.eliminarReseña(reseña);
        assertTrue(libro.getReseñas().isEmpty(), "La reseña no fue eliminada correctamente");
    }

}
