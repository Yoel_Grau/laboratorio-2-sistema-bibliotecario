package SistemaBibliotecario;

import java.util.ArrayList;
import java.util.List;

public class Biblioteca {
    private String nombre;
    private String direccion;
    private List<Libro> catalogoLibros;
    private List<Usuario> usuariosRegistrados;

    public Biblioteca(String nombre, String direccion) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.catalogoLibros = new ArrayList<>();
        this.usuariosRegistrados = new ArrayList<>();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public List<Libro> getCatalogoLibros() {
        return catalogoLibros;
    }

    public List<Usuario> getUsuariosRegistrados() {
        return usuariosRegistrados;
    }

    public boolean agregarLibro(Libro libro) {
        if (libro == null) {
            System.out.println("El libro no puede ser nulo.");
            return false;
        }
        catalogoLibros.add(libro);
        return true;
    }

    public boolean eliminarLibro(String idLibro) {
        Libro libro = buscarLibro(idLibro);
        if (libro == null) {
            System.out.println("Libro no fue encontrado.");
            return false;
        }
        catalogoLibros.remove(libro);
        return true;
    }

    public Libro buscarLibro(String idLibro) {
        for (Libro libro : catalogoLibros) {
            if (libro.getIdLibro().equals(idLibro)) {
                return libro;
            }
        }
        return null;
    }

    public boolean registrarUsuario(Usuario usuario) {
        if (usuario == null) {
            System.out.println("El usuario no puede ser nulo.");
            return false;
        }
        if (usuariosRegistrados.contains(usuario)) {
            System.out.println("El usuario ya esta registrado.");
            return false;
        }
        usuariosRegistrados.add(usuario);
        return true;
    }

    public boolean eliminarUsuario(String idUsuario) {
        Usuario usuario = buscarUsuario(idUsuario);
        if (usuario == null) {
            System.out.println("Usuario no encontrado.");
            return false;
        }
        usuariosRegistrados.remove(usuario);
        return true;
    }

    public Usuario buscarUsuario(String idUsuario) {
        for (Usuario usuario : usuariosRegistrados) {
            if (usuario.getIdUsuario().equals(idUsuario)) {
                return usuario;
            }
        }
        return null;
    }
}





