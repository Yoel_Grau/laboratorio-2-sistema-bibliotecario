package SistemaBibliotecario;

import java.util.ArrayList;
import java.util.List;

public class Libro {
    private String idLibro;
    private String titulo;
    private String genero;
    private String autor;
    private String estado;
    private int cantidad;
    private List<Reseña> reseñas;

    public Libro(String idLibro, String titulo, String genero, String autor, int cantidad) {
        this.idLibro = idLibro;
        this.titulo = titulo;
        this.genero = genero;
        this.autor = autor;
        this.estado = "Disponible";
        this.cantidad = cantidad;
        this.reseñas = new ArrayList<>();
    }

    public String getIdLibro() {
        return idLibro;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getGenero() {
        return genero;
    }

    public String getAutor() {
        return autor;
    }

    public String getEstado() {
        return estado;
    }

    public int getCantidad() {
        return cantidad;
    }

    public List<Reseña> getReseñas() {
        return new ArrayList<>(reseñas);
    }

    public void actualizarEstado(String nuevoEstado) {
        this.estado = nuevoEstado;
    }

    public void anadirCopia() {
        this.cantidad++;
    }

    public void eliminarLibro() {
        if (this.cantidad <= 0) {
            System.out.println("No hay libros disponibles para eliminar.");
            return;
        }
        this.cantidad--;
    }

    public boolean estaDisponible() {
        return "Disponible".equals(estado) && cantidad > 0;
    }

    public void añadirReseña(Reseña reseña) {
        reseñas.add(reseña);
    }

    public void eliminarReseña(Reseña reseña) {
        reseñas.remove(reseña);
    }

}






