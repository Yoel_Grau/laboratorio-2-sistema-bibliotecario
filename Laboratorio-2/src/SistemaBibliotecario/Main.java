package SistemaBibliotecario;


public class Main {
    public static void main(String[] args) throws Exception {
        System.out.println("Biblioteca");

        Biblioteca biblioteca = new Biblioteca("Biblioteca", "100 Main St.");

        System.out.println("Creando usuarios");
        UsuarioAdministrador administrador = new UsuarioAdministrador("A01", "Admin", "Direccion 2");
        UsuarioCliente cliente1 = new UsuarioCliente("U01", "User1", "Direccion 3");
        UsuarioCliente cliente2 = new UsuarioCliente("U02", "User2", "Direccion 4");

        System.out.println("Añadiendo libros al catalogo");
        Libro libro1 = new Libro("L001", "1984", "Distopia", "George Orwell", 3);
        Libro libro2 = new Libro("L002", "To Kill a Mockingbird", "Clásico", "Harper Lee", 2);
        biblioteca.agregarLibro(libro1);
        biblioteca.agregarLibro(libro2);

        System.out.println("Registrando usuarios en la biblioteca");
        administrador.registrarUsuario(biblioteca, cliente1);
        administrador.registrarUsuario(biblioteca, cliente2);

        System.out.println("Realizando prestamos de libros");
        Prestamo prestamo1 = new Prestamo(cliente1, libro1);
        System.out.println("Préstamo realizado a " + cliente1.getNombre() + " para el libro " + libro1.getTitulo());
        Prestamo prestamo2 = new Prestamo(cliente2, libro2);
        System.out.println("Préstamo realizado a " + cliente2.getNombre() + " para el libro " + libro2.getTitulo());

        System.out.println("Cliente1 añadio una reseña para el libro1");
        Reseña reseña1 = new Reseña("Una obra maestra atemporal", 5, cliente1, libro1);
        cliente1.añadirReseña(reseña1);
        System.out.println("Reseña añadida: " + reseña1.getTexto());

        System.out.println("Cliente1 reservo el libro2");
        ReservarLibro sistemaReservas = new ReservarLibro();
        sistemaReservas.añadirReserva(cliente1, libro2);
        System.out.println("Reserva realizada para el libro " + libro2.getTitulo());

        Notificaciones notificaciones = new Notificaciones();
        notificaciones.enviarRecordatorioDevolucion(prestamo1);
        notificaciones.enviarRecordatorioDevolucion(prestamo2);

        Sanciones sanciones = new Sanciones();
        Sanciones.Sancion sancion1 = new Sanciones.Sancion(cliente2, "Retraso en la devolución", 50);
        sanciones.añadirSancion(sancion1);
        System.out.println("Sanción añadida para " + cliente2.getNombre() + ": " + sancion1.getMotivo());

        System.out.println("Finalizando préstamos");
        prestamo1.finalizarPrestamo();
        System.out.println("Préstamo del libro " + libro1.getTitulo() + " finalizado.");
        prestamo2.finalizarPrestamo();
        System.out.println("Préstamo del libro " + libro2.getTitulo() + " finalizado.");
    }
}






