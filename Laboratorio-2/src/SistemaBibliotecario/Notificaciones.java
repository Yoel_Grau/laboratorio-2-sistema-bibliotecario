package SistemaBibliotecario;

public class Notificaciones {

    public void enviarRecordatorioDevolucion(Prestamo prestamo) {
        if (prestamo == null) {
            System.out.println("Error: El prestamo es nulo.");
            return;
        }

        Usuario usuario = prestamo.getUsuario();
        Libro libro = prestamo.getLibro();
        String mensaje = "Recordatorio: El libro '" + libro.getTitulo() + "' prestado debe ser devuelto pronto.";

        System.out.println("Enviando recordatorio a " + usuario.getNombre() + ": " + mensaje);
    }

    public void enviarNotificacionReserva(Usuario usuario, Libro libro) {
        if (usuario == null || libro == null) {
            System.out.println("Error: Usuario o libro nulo.");
            return;
        }

        String mensaje = "Notificación: El libro '" + libro.getTitulo() + "' ha sido reservado para usted.";
        System.out.println("Enviando notificación a " + usuario.getNombre() + ": " + mensaje);
    }

    public void enviarAlertas(String mensaje) {
        System.out.println("Enviando alerta: " + mensaje);
    }

    public void enviarNotificacionSancion(Sanciones.Sancion sancion) {
        if (sancion == null) {
            System.out.println("Error: La sanción es nula.");
            return;
        }
        Usuario usuario = sancion.getUsuario();
        String mensaje = "Notificación de sanción: " + sancion.getMotivo() + ". Monto: " + sancion.getMonto();
        System.out.println("Enviando notificación de sanción a " + usuario.getNombre() + ": " + mensaje);
    }

}







