package SistemaBibliotecario;

public class Prestamo {
    private int duracionMaximaPrestamo = 7;
    private Libro libro;
    private Usuario usuario;
    private int diasPrestamo;
    private Integer diasDevolucion;

    public Prestamo(Usuario usuario, Libro libro) {
        if (usuario == null || libro == null || libro.getEstado().equals("Prestado")) {
            return;
        }

        this.usuario = usuario;
        this.libro = libro;
        this.diasPrestamo = duracionMaximaPrestamo;
        this.diasDevolucion = null;

        libro.actualizarEstado("Prestado");
        usuario.prestarLibro(libro);
    }

    public boolean renovarPrestamo() {
        if (libro.getEstado().equals("Prestado")) {
            diasPrestamo = duracionMaximaPrestamo;
            return true;
        }
        return false;
    }

    public void finalizarPrestamo() throws Exception {
        libro.actualizarEstado("Disponible");
        usuario.devolverLibro(libro);
        diasDevolucion = diasPrestamo;
    }

    public Libro getLibro() {
        return libro;
    }

    public void setLibro(Libro libro) {
        this.libro = libro;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public int getDiasPrestamo() {
        return diasPrestamo;
    }

    public void setDiasPrestamo(int diasPrestamo) {
        this.diasPrestamo = diasPrestamo;
    }

    public Integer getDiasDevolucion() {
        return diasDevolucion;
    }

    public void setDiasDevolucion(Integer diasDevolucion) {
        this.diasDevolucion = diasDevolucion;
    }
}


