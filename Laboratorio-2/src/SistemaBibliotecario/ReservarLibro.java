package SistemaBibliotecario;

import java.util.ArrayList;
import java.util.List;

public class ReservarLibro {
    private List<Prestamo> reservas;

    public ReservarLibro() {
        this.reservas = new ArrayList<>();
    }

    public void añadirReserva(Usuario usuario, Libro libro) throws Exception {
        Prestamo nuevaReserva = new Prestamo(usuario, libro);
        reservas.add(nuevaReserva);
    }

    public void cancelarReserva(Prestamo reserva) throws Exception {
        reserva.getLibro().actualizarEstado("Disponible");
        reservas.remove(reserva);
    }

    public List<Prestamo> getReservas() {
        return new ArrayList<>(reservas);
    }

}




