package SistemaBibliotecario;

public class Reseña {
    private String texto;
    private int calificacion;
    private Usuario usuario;
    private Libro libro;

    public Reseña(String texto, int calificacion, Usuario usuario, Libro libro) {
        if(calificacion < 1 || calificacion > 5 || usuario == null || libro == null) {
            System.out.println("La Reseña no pudo ser creada");
            return;
        }

        this.texto = texto;
        this.calificacion = calificacion;
        this.usuario = usuario;
        this.libro = libro;

        usuario.añadirReseña(this);
        libro.añadirReseña(this);
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public int getCalificacion() {
        return calificacion;
    }

    public boolean setCalificacion(int calificacion) {
        if (calificacion < 1 || calificacion > 5) {
            return false;
        }
        this.calificacion = calificacion;
        return true;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public Libro getLibro() {
        return libro;
    }

    public void modificarReseña(String nuevoTexto, int nuevaCalificacion) throws Exception {
        setTexto(nuevoTexto);
        setCalificacion(nuevaCalificacion);
    }

    public void eliminarReseña() {
        usuario.eliminarReseña(this);
        libro.eliminarReseña(this);
    }
}







