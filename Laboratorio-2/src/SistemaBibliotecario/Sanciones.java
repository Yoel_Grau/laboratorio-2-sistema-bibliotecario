package SistemaBibliotecario;

import java.util.ArrayList;
import java.util.List;

public class Sanciones {
    private List<Sancion> registrosDeSanciones;

    public static class Sancion {
        private Usuario usuario;
        private String motivo;
        private double monto;

        public Sancion(Usuario usuario, String motivo, double monto) {
            this.usuario = usuario;
            this.motivo = motivo;
            this.monto = monto;
        }

        public Usuario getUsuario() {
            return usuario;
        }

        public String getMotivo() {
            return motivo;
        }

        public void setMotivo(String motivo) {
            this.motivo = motivo;
        }

        public double getMonto() {
            return monto;
        }

        public void setMonto(double monto) {
            this.monto = monto;
        }
    }

    public Sanciones() {
        this.registrosDeSanciones = new ArrayList<>();
    }

    public boolean añadirSancion(Sancion sancion) {
        if (sancion == null || sancion.getUsuario() == null) {
            return false;
        }
        registrosDeSanciones.add(sancion);
        return true;
    }

    public boolean eliminarSancion(Sancion sancion) {
        if (sancion == null || !registrosDeSanciones.contains(sancion)) {
            return false;
        }
        registrosDeSanciones.remove(sancion);
        return true;
    }

    public List<Sancion> consultarSanciones(Usuario usuario) {
        List<Sancion> sancionesUsuario = new ArrayList<>();
        for (Sancion sancion : registrosDeSanciones) {
            if (sancion.getUsuario().equals(usuario)) {
                sancionesUsuario.add(sancion);
            }
        }
        return sancionesUsuario;
    }
}






