package SistemaBibliotecario;

import java.util.ArrayList;
import java.util.List;

public class Usuario {
    private String idUsuario;
    private String nombre;
    private String direccion;
    private List<Libro> librosPrestados;
    private List<Reseña> reseñas;

    public Usuario(String idUsuario, String nombre, String direccion) {
        this.idUsuario = idUsuario;
        this.nombre = nombre;
        this.direccion = direccion;
        this.librosPrestados = new ArrayList<>();
        this.reseñas = new ArrayList<>();
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public List<Libro> getLibrosPrestados() {
        return librosPrestados;
    }

    public void prestarLibro(Libro libro){
        librosPrestados.add(libro);
    }

    public void devolverLibro(Libro libro) {
        librosPrestados.remove(libro);
    }

    public void añadirReseña(Reseña reseña) {
        if (reseña != null && !reseñas.contains(reseña)) {
            reseñas.add(reseña);
        }
    }

    public void eliminarReseña(Reseña reseña) {
        reseñas.remove(reseña);
    }
}


