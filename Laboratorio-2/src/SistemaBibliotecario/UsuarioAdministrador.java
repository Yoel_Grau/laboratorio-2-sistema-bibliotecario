package SistemaBibliotecario;

public class UsuarioAdministrador extends Usuario {

    private int multaPorDia = 300;

    public UsuarioAdministrador(String idUsuario, String nombre, String direccion) {
        super(idUsuario, nombre, direccion);
    }

    public boolean registrarUsuario(Biblioteca biblioteca, Usuario usuario) {
        if (biblioteca == null || usuario == null) {
            return false;
        }
        return biblioteca.registrarUsuario(usuario);
    }

    public boolean agregarLibroBiblioteca(Biblioteca biblioteca, Libro libro) {
        if (biblioteca == null || libro == null) {
            return false;
        }
        return biblioteca.agregarLibro(libro);
    }

    public boolean eliminarLibroBiblioteca(Biblioteca biblioteca, String idLibro) {
        if (biblioteca == null || idLibro == null || idLibro.isEmpty()) {
            return false;
        }
        return biblioteca.eliminarLibro(idLibro);
    }

    public boolean gestionarUsuarios(Biblioteca biblioteca, Usuario usuario) {
        if (biblioteca == null || usuario == null) {
            return false;
        }
        return biblioteca.registrarUsuario(usuario);
    }

    public boolean asignarMulta(Usuario usuario, int diasDeRetraso) {
        if (usuario == null || diasDeRetraso < 0) {
            return false;
        }

        if (diasDeRetraso > 0) {
            long multaTotal = diasDeRetraso * multaPorDia;
            System.out.println("Multa asignada a " + usuario.getNombre() + ": " + multaTotal + " pesos por " + diasDeRetraso + " días de retraso.");
            return true;
        } else {
            System.out.println("No hay multa ya que no hubo retraso en la entraga del libro.");
            return false;
        }
    }
}





