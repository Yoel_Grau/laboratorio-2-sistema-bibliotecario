package SistemaBibliotecario;

import java.util.List;

public class UsuarioCliente extends Usuario {
    public UsuarioCliente(String idUsuario, String nombre, String direccion) {
        super(idUsuario, nombre, direccion);
    }

    public void actualizarDatos(String nombre, String direccion) {
        setNombre(nombre);
        setDireccion(direccion);
    }

    public List<Libro> verLibrosPrestados() {
        return getLibrosPrestados();
    }
}





